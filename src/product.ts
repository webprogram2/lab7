import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productsRepository = AppDataSource.getRepository(Product)
    // const product = new Product()
    // product.name = "ข้าวหน้าเป็ด";
    // product.price = 50;
    // await productsRepository.save(product)
    // console.log("Saved a new product with id: " + product.id)

    // console.log("Loading products from the database...")
    const products = await productsRepository.find()
    console.log("Loaded products: ", products)

    const updatedProduct = await productsRepository.findOneBy({id: 1})
    console.log(updatedProduct)
    updatedProduct.price=80
    await productsRepository.save(updatedProduct)

}).catch(error => console.log(error))
